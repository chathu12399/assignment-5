#include <stdio.h>

#define performancecost 500
#define perattendee 3

int attendance(int t)
{
    return 120-(t-15)/5*20;
}

int income(int t)
{
    return attendance(t)*t;
}

int expenditure(int t)
{
    return attendance(t)*perattendee+performancecost;
}

int profit(int t)
{
    return income(t)-expenditure(t);
}

int main()
{
    int t;
    for(t=5; t<50; t+=5)
    {
        printf("When your ticket price is rs.%d your profit is rs.%d \n", t, profit(t));
    }
    return 0;
}
